package main

import(
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

)

const url_Prifix = "http://"

func main(){

	for _, url := range os.Args[1:]{
		if !strings.HasPrefix(url,url_Prifix){
			url = url_Prifix + url
		}

		resp, err := http.Get(url)
		if err != nil{
			fmt.Fprintf(os.Stderr, "fetch:%v\n",err)
			os.Exit(1)
		}

		_, err = io.Copy(os.Stdout, resp.Body)
		resp.Body.Close()
		if err != nil{
			fmt.Fprintf(os.Stderr, "fetch:%v\n",err)
			os.Exit(1)
		}
	}
}
