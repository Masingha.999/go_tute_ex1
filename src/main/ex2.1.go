package main

import(
	"fmt"
)

type Celsius float64
type Fhrenheit float64
type Kelvin float64

const(
	AbsolutezeroC Celsius = -273.15
	FreezingC Celsius = 0
	BoilingC Celsius = 100
)

func(c Celsius)String()string{
	return fmt.Sprintf("%gºC", c)
}

func (f Fhrenheit)String() string{
	return fmt.Sprintf("%gºC", f)
}

func (k Kelvin)String()string{
	return fmt.Sprintf("%gºK", k)
}

func CtoF (c Celsius)Fhrenheit{
	return Fhrenheit((c*9/5)+32)
}

func FtoC(f Fhrenheit) Celsius {
	return Celsius((f - 32) * 5 / 9)
}

func CtoK(c Celsius) Kelvin     {
	return Kelvin(c - AbsolutezeroC)
}

func KtoC(k Kelvin) Celsius     {
	return Celsius(k) + AbsolutezeroC
}

func FtoK(f Fhrenheit) Kelvin  {
	return CtoK(FtoC(f))
}

func KtoF(k Kelvin) Fhrenheit  {
	return CtoF(KtoC(k))
}

func main(){
	fmt.Printf("0K = %gºC\n", KtoC(0))
	fmt.Printf("0ºC = %gK\n", CtoK(0))
	fmt.Printf("0ºF = %gK\n", FtoK(0))
}